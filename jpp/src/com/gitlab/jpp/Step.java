/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Represent a Step of a Pipeline.
 * Take care of the synchronization of all the steps of a pipeline.
 * @author Rémi Segretain
 */
public abstract class Step<Output> {
    
    /**
     * The pipeline that contain the step.
     */
    protected final Pipeline pipeline;
    
    /**
     * list of this step parents in the pipeline graph of step.
     */
    final List<Step> previousSteps;
    
    /**
     * list of this step childs in the pipeline graph of step.
     */
    final List<Step> followingSteps;
    
    /**
     * Flag saving the state of the step for one pipeline execution.
     * Over or not.
     */
    protected volatile boolean stepOver;
    
    /**
     * Module define by the user, in charge of handling the output datas of the step.
     */
    protected final OutputHandler<Output> outputHandler;

    public Step(Pipeline pipeline, OutputHandler outputHandler, Step... previousSteps) {
        this.pipeline = pipeline;
        this.outputHandler = outputHandler;
        this.previousSteps = new ArrayList<>(previousSteps.length);
        this.previousSteps.addAll(Arrays.asList(previousSteps));
        this.followingSteps = new ArrayList<>(0);
        this.stepOver = false;
    }
    
    /**
     * Give the number of tasks waiting to be executed by this step.
     * @return
     */
    abstract int getWaitingTasksNumber();
    
    /**
     * Signal to previous steps that this one is idle, so they could submit tasks to the pipeline
     */
    final protected void signalStepIsIdle() {
        this.previousSteps.stream().forEach((step) -> step.trySubmitTasks());
    }
    
    /**
     * Allow to ask to the step to try and execute tasks
     */
    abstract void trySubmitTasks();
    
    /**
     * Give the status of the step for the current pipeline execution, over or not.
     * @return true if the step is over.
     */
    abstract boolean isStepOver();
    
    /**
     * Perform a check on the step to detect if it is over.
     */
    synchronized void checkIfTheStepIsOver() {
        if (this.isStepOver() && this.stepOver == false) {
            this.onStepOver();
        }
    }
    
    /**
     * Perform the required action when the step is over
     */
    synchronized final void onStepOver(){
        this.stepOver = true;
        this.followingSteps.forEach((step) -> {
            step.checkIfTheStepIsOver();
        });
        this.pipeline.onStepOver();
    }
    
    /**
     * Gives monitoring infos concerning the step.
     * @return infos as string.
     */
    abstract String getMonitoringMessage();
    
    /**
     * Reset the step and his modules to be ready for the next pipeline execution
     */
    void reset() {
        this.stepOver = false;
        this.outputHandler.reset();
    }
}
