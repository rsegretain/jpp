/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

import com.gitlab.jpp.parameters.Pair;
import java.util.HashMap;

/**
 * A OutputMerger is one possible step of a pipeline.
 * It store and aggregate data based on a ID.
 * When two linked data are received
 * they are aggregated and handed to the rest of the pipeline.
 * @author Rémi Segretain
 */
public final class OutputsMerger<Id, OutputA, OutputB> extends Step<Pair<OutputA,OutputB>> {

    /**
     * The maps that stored the data waiting to be aggregated.
     */
    private final HashMap<Id, OutputA> mapA;
    private final HashMap<Id, OutputB> mapB;

    /**
     * Private constructor, must use the Builder.
     * @param pipeline
     * @param outputHandler
     * @param previousSteps
     */
    private OutputsMerger(Pipeline pipeline, OutputHandler outputHandler, Step... previousSteps) {
        super(pipeline, outputHandler, previousSteps);
        this.mapA = new HashMap<>();
        this.mapB = new HashMap<>();
    }
    
    /**
     * Give the first piece of data to the merger.
     * @param id the id of the data
     * @param data the data to aggregate
     */
    public synchronized void handleDataA(Id id, OutputA data) {
        this.mapA.put(id, data);
        this.checkId(id);
    }
    
    /**
     * Give the second piece of data to the merger.
     * @param id the ID of the data
     * @param data the data to aggregate
     */
    public synchronized void handleDataB(Id id, OutputB data) {
        this.mapB.put(id, data);
        this.checkId(id);
    }
    
    /**
     * Perform a check if the twos data pieces of a ID were receives by the merger.
     * @param id the ID to check
     */
    private synchronized void checkId(Id id) {
        if (this.mapA.containsKey(id) && this.mapB.containsKey(id)) {
            this.outputHandler.handle(new Pair<>(this.mapA.get(id), this.mapB.get(id)));
            this.mapA.remove(id);
            this.mapB.remove(id);
            this.checkIfTheStepIsOver();
        }
    }

    /**
     * Return the number of tasks waiting to be executed by the followings steps.
     * @return
     */
    @Override
    int getWaitingTasksNumber() {
        return this.followingSteps.stream().mapToInt((step) -> step.getWaitingTasksNumber()).sum();
    }

    /**
     * Reset the merger to be ready for the next pipeline execution.
     */
    @Override
    synchronized void reset() {
        super.reset();
        this.mapA.clear();
        this.mapB.clear();
    }

    /**
     * Give the status of the step for the current pipeline execution, over or not.
     * @return
     */
    @Override
    boolean isStepOver() {
        return (
            this.stepOver
            || (
                this.previousSteps.stream()
                        .map((step) -> step.isStepOver())
                        .noneMatch((isStepOver) -> isStepOver == false)
                && this.mapA.isEmpty()
                && this.mapB.isEmpty()
            )
        );
    }

    /**
     * As a merger don't process tasks, directly signal to the parents steps that this one is idle.
     */
    @Override
    void trySubmitTasks() {
        this.signalStepIsIdle();
    }

    /**
     * Gives monitoring infos concerning the step.
     * @return infos as string.
     */
    @Override
    String getMonitoringMessage() {
        return "OutputMerger " + (this.stepOver ? "[DONE]" : "RUNNING") + " :\tA : " + this.mapA.size() + "\tB : " + this.mapB.size() + '\n';
    }
    
    /**
     * Build a outputMerger with the parameters specified by the user and fill the other with default values.
     * @param <Id>
     * @param <OutputA>
     * @param <OutputB>
     */
    public static class Builder<Id, OutputA, OutputB> {
        
        /**
         * All the parameters of the OutputsMerger constructor
         * are mirrored as attributes if the builder.
         */
        private final Pipeline pipeline;
        private final Step[] previousSteps;        
        private OutputHandler<Pair<OutputA, OutputB>> outputHandler;

        /**
         * Builder constructor.
         * Required the mandatory parameters for a OutputsMerger
         * and set the others to defaults values.
         * @param pipeline
         * @param previousSteps
         */
        public Builder(Pipeline pipeline, Step... previousSteps) {
            this.pipeline = pipeline;
            this.previousSteps = previousSteps;
            this.outputHandler = new OutputHandler<Pair<OutputA, OutputB>>() {
                                    @Override
                                    public void handle(Pair<OutputA, OutputB> output) {}
                                };
        }

        /**
         * Set the outputHandler.
         * @param outputHandler
         * @return the builder to chain the setters calls.
         */
        public Builder setOutputHandler(OutputHandler<Pair<OutputA, OutputB>> outputHandler) {
            this.outputHandler = outputHandler;
            return this;
        }
        
        /**
         * Build a OutputsMerger with the specified parameters.
         * @return a new instances of OutputsMerger.
         */
        public OutputsMerger<Id, OutputA, OutputB> build() {
            OutputsMerger outputsMerger = new OutputsMerger(this.pipeline, this.outputHandler, this.previousSteps);
            this.pipeline.addStep(outputsMerger);
            return outputsMerger;
        }
    }
}
