/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */

import com.gitlab.jpp.Job;
import com.gitlab.jpp.Pipeline;
import com.gitlab.jpp.Processor;
import com.gitlab.jpp.TaskHolder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Test edge case where the end of a step is never checked and prevent the
 * pipeline execution to end
 *
 * @author Rémi Segretain
 */
public class TestIdleJob extends Pipeline<Void, Void> {

	private final Job a;
	private final Job b;

	public TestIdleJob() {
		super(Runtime.getRuntime().availableProcessors());

		this.a = new Job.JobBuilder<>(
				this,
				new Processor<Void, Void>() {
			@Override
			public Void process(Void input) {
				return null;
			}

			@Override
			public void reset() {
			}
		}
		)
				.setTaskSubmitter(new TaskHolder(getNewReusableCountDownLatch(
						0)))
				.build();

		this.b = new Job.JobBuilder<>(this, new Processor<Void, Void>() {
			@Override
			public Void process(Void input) {
				return null;
			}

			@Override
			public void reset() {

			}
		}, a)
				.setTaskSubmitter(new TaskHolder(a.getEndOfJobLatch()))
				.build();
	}

	@Override
	protected void initialTasks() {
		a.buildNewTask(null);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException ex) {
			Logger.getLogger(TestIdleJob.class.getName()).log(Level.SEVERE, null,
					ex);
		}
	}

	@Override
	protected void reset() {
	}

	public static void main(String[] args) {

		System.setProperty(MONITORING_PROPERTY, "true");
		System.setProperty(MONITORING_PROPERTY_SETTING, "5");

		TestIdleJob pipeline = new TestIdleJob();

		try {
			//for (int i = 0; i < 10000; i++) {
			//System.out.println("\n##### " + i + "\n");
			pipeline.executeFor(null);
			//}
		} catch (InterruptedException | IllegalStateException ex) {
			Logger.getLogger(TestOutputsMergerPipeline.class.getName()).log(
					Level.SEVERE, null, ex);
		}

		pipeline.halt();
	}
}
