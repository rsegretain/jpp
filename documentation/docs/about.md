# About
---

# Version

JPP last version is V1.1.

## V1.1 Changelog

**Features :**

- Added the concept of Step that abstract the different components of a Pipeline : a Job is a Step.
- Added OutputMerger, a new type of Step made to aggregate on the fly Tasks coming from two branches of Steps.
- Added a Job setting to choose the maximum number of Tasks that can be executed simulteanously for this Job (default value set to the number of threads allocated for the Pipeline). Useful for throttling a Job output that generate a lot of Tasks.
- Added utility classes that wrap basics types to define resettable Pipeline/Job/Processor parameters.
- Removed Modules setters for Job, resettable parameters should be used instead of recreatings Modules on Pipeline reset.
- Added informations to the execution Monitor :
	- The quantity of RAM consummed by/available for the Pipeline.
	- For each Job, the numbers of Tasks being currently executed by the Pipeline.

**Performances :**

- Improved the automatic Task flow management, the priority of the deepest Steps is now way better respected.

**Quality of life :**

- Changed the manner of using ReusableCountDownLatch : they are now managed by the Pipeline. This change simplify their usage and allow to fix a related bug during the Pipeline reset sequence.
- Changed the threads used by a Pipeline to make them daemons. This remove the mandatory user call to the Pipeline halt method.
- Initials Jobs no longer require to declare a TaskHolder on Pipeline startSignal. One is now set automatically if the user don't specify any.
- Added error print if a exception is throw by the Pipeline itself.

**Bugs :**

- Fixed a race condition that cause null Tasks to be added to the Pipeline.
- Fixed bug that caused the endOfJob sequence to be runned multiple times.
- Fixed several bugs that would make a Pipeline execution hold indefinitely.


# Contacts

Use the [issues](https://gitlab.com/rsegretain/jpp/-/issues) page on gitlab.

By mail at r&#60;dot&#x3e;segretain&#60;at&#x3e;hotmail&#60;dot&#x3e;fr , ***Please, put "[JPP]" in the object***.

# License

!!! note ""
	MIT License

	Copyright (c) 2021 Rémi Segretain

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
