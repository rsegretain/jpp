---
title: 'JPP : Java-Parallel-Pipeline'
tags:
  - Java
  - parallelism
authors:
  - name: Rémi Segretain
    orcid: 0000-0003-3044-9792
    affiliation: 1
affiliations:
 - name: Univ. Grenoble Alpes, CNRS, Grenoble INP, TIMC, 38000 Grenoble, France
   index: 1
date: 16 april 2021
bibliography: paper.bib
---

# Summary

`JPP` is a Java library designed to help the user build a parallelized process that automatically distributes the workload over the available CPU cores.

# Statement of need

Handmade parallelization requires rigour and investment to do it properly.
`JPP` aims to greatly reduce the time, the effort and the knowledge needed by the programmer to build clear and efficient parallel programs.

In the context of scientific research, code specifications can change rapidly.
Maintaining correct parallel scientific softwares under these conditions ranges from difficult to impossible.
The primary objective of `JPP` is to make it possible and accessible even for people not familiar with parallelism.
It is easy for a programmer, not only beginners, to lose precious time in developpment maze.
The Java language is a good choice because it offers a fair middle ground between performances and developpment ease, particularly for software developpment.

Concretly `JPP` offers a framework where the user need to define his process as a pipeline made from a chain (or a tree) of blocks of code.
Each one of them is a step of the process stored in a independent and, consequently, reusable block.
This allows to limit the impact of code refactoring but also to build new processes very quickly using existing blocks.
It allows a great code flexibility, modularity and reusability, which is welcome for research work.
A block will typically do what is done inside large loops in a linear code.
In a pipeline, each block execution will correspond to one loop iteration.
But at runtime, instead of executing one loop iteration after another, multiple block executions run concurrently, thus parallelizing the loop. Furthermore, once the flow of data from block to block is set up, all of them can be executed simultaneously.

To start a new project or migrate existing code, all that is needed from the user is to extend and implement the classes and interfaces given by `JPP` and fill them with his code, thereby defining the blocks and how they must be linked.
For more details, user guide and documentation are available here : [https://rsegretain.gitlab.io/jpp/](https://rsegretain.gitlab.io/jpp/)

# Mention

`JPP` was originally developped to help during the development of scientific computations performed for this scientific article [@S:CN2020]. A description of this implementation is available here [@S:BR2020].

# Acknowledgements

The author would like to thank the IDEX program of the University Grenoble Alpes for its support through the projects COOL : this work is supported by the French National Research Agency in the framework of the Investissements d’Avenir program (ANR-15-IDEX-02).
This work is also supported by the Innovation in Strategic Research program of the University Grenoble Alpes.
Project PI : N. Glade, TIMC-IMAG laboratory, Univ. Grenoble Alpes, France.

# References

Through its technical concepts, `JPP` addresses similar problems to `OpenMP` : loops and tasks parallelization.
However, they are radically different. `OpenMP` is a mature and widely used HPC standard, whereas `JPP` targets a different set of uses : quick developements for scientific research and software-embedded computations.
The small size of the library make it possible to extend or adapt it to better meet specifics needs or hardware.
