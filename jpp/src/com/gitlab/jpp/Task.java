/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

/**
 * A task is small execution unit.
 * It contain the parameters required for a execution of a job's processor.
 * @author Rémi Segretain
 * @param <Input> The type of input parameter required by the Job and his Processor.
 */
class Task<Input> implements Runnable {

    /**
     * The job to call when the task is executed.
     */
    private final Job job;

    /**
     * The input parameter of the job's processor execution.
     */
    private final Input input;
    
    protected Task(Job job, Input input) {
        this.job = job;
        this.input = input;
    }
    
    /**
     * Execute the task.
     */
    @Override
    public void run() {
        this.job.executeFor(input);
    }

    @Override
    public String toString() {
        return "Task{" + "job=" + job + "input=" + input + '}';
    }
}
