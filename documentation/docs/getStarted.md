# Get Started
---

Concretly JPP give to its users classes and interfaces to extends/implements guiding you to organize your code and correctly assemble components together.

!!! note
	![Icon of direct javadoc link](./images/doc_icon.png "Icon of direct javadoc link") : Directs links to the corresponding javadoc.

## Create a pipeline

Create your pipeline by extending the [Pipeline ![Pipeline javadoc](./images/doc_icon.png)](../javadoc/com/gitlab/jpp/Pipeline.html "Pipeline javadoc") class.
That where you will define your process by piecing together jobs and theirs modules.

The Pipeline class have two generic types you need to define : `Input` and `Output`.

* `Input` : The type of the parameters required for a execution of your pipeline. You will need to provide a `Input` instance when calling the execution method.
* `Output` : The type of the data produced by a execution of your pipeline. You will get a `Output` instance returned when calling the execution method.

There is at least two methods to implement :

* `constructor` :
	* Initiate globals structures
	* Initiate jobs (more details [here](getStarted.md#create-jobs)), always following the order : Parent -> Child.

	The parameters given to your constructor should be used only to create the pipeline, not for its executions.

	!!! hint "Choosing the number of threads"
		When calling the pipeline `super` constructor, you specify the number of threads you want to allocate to the pipeline. **The better is not necessarily the most**. A basic choice would be to take as much as the number of core/thread offered by your CPU. In practice, the best choice is different for every software, so investing a bit of time to explore diverse configurations can be beneficial.

* `reset()` : Re-initialize every global structure that need it in order to insure a clean state for the following execution. If you have created any [ReusableCountDownLatch](#reusablecountdownlatch "ReusableCountDownLatch documentation") you should reset them here.

* `initialTasks()` : It's where you set up the first tasks for from the pipeline input for the root jobs.

Optionnaly you can override those methods :

* `after()` : Allow to define a global processing executed as the last step of the pipeline execution, after the end of every job.
* `getMonitoringMessage()` : Allow to add a custom message to be displayed by the monitor if it is enabled.

## Create Processors

A processor is a class that implements the interface [Processor ![Processor javadoc](./images/doc_icon.png "Processor javadoc")](../javadoc/com/gitlab/jpp/Processor.html "Processor javadoc").

The Processor interface have two generic types you need to define : `Input` and `Output`.

* `Input` : The type of the parameters required for a execution of the processor. A `Input` instance is required when creating a task for the processor.
* `Output` : The type of the data produced by a execution of the processor. You will get a `Output` instance returned after a the execution of a task of the processor. The OutputHandler of the processor must be parameterized with the same generic type.

Your processor may need configurations parameters common to all the tasks, in this case specify them in the constructor. For examples the path to a ressource file or a particuliar execution mode.

Keep in mind that a Processor must be totally independent in order to be usable across multiple pipelines.

## Create jobs [![Job javadoc](./images/doc_icon.png)](../javadoc/com/gitlab/jpp/Job.html "Job javadoc")

The Job class is not extendable, each instances is configured through the modules : Processor, OutputHandler, ExceptionHandler, TaskSubmitter, EndOfJobAction. In order to synchronize properly those modules, a job need to know the `Input` and `Output` types of the processor, so the Job class is also generic.

### JobBuilder [![JobBuilder javadoc](./images/doc_icon.png)](../javadoc/com/gitlab/jpp/Job.JobBuilder.html "JobBuilder javadoc")

The constructor method is not accessible, jobs must be build through the given builder : JobBuilder. A job require at least two parameters, the pipeline and a processor, all others modules will be set to default implementation if not specified.

!!! note "Builder"
	A Builder is a common design pattern useful when most of the constructor parameters are optionnals or can be set to known default value. It enable to build a object specifying only the parameters you want.

The builder constructor require the mandatory parameters or thoses for which there is no default values. The optionnal parameters can then be set by chaining calls to setters. The last method of the chain must be `build()` that will construct and return the Job.

```java
Job job = new Job.JobBuilder(pipeline, processor)
			.setTaskSubmitter(taskSubmitter)
			//....
			.setEndOfJobAction(oneTimeAction)
			.build();
```

### Job modules

#### TaskSubmitter [![TaskSubmitter javadoc](./images/doc_icon.png)](../javadoc/com/gitlab/jpp/TaskSubmitter.html "TaskSubmitter javadoc")

Handle the submission of a new task to a Job. It's the only modules that can't be customized by the user. There is two implementations available :

* Default : directly submit the tasks with no further processing.
* [TaskHolder ![TaskHolder javadoc](./images/doc_icon.png)](../javadoc/com/gitlab/jpp/TaskHolder.html "TaskHolder javadoc") : Hold the incoming tasks in a queue until a signal is receveid through a given
[ReusableCountDownLatch](getStarted.md#reusablecountdownlatch "ReusableCountDownLatch documentation").
Then all holded tasks are submitted and any future one will be directly submitted.

	The ReusableCountDownLatch used can be a custom one or one of those provided by JPP components :

	* Jobs `getEndOfJobLatch()`: signal when the job end.
	* Pipeline `startSignal` : signal when the pipeline is ready for the execution to begin.

	!!! warning
		Always use a TaskHolder on `startSignal` for the firsts jobs, otherwise the execution will start before the pipeline is ready.

#### OutputHandler [![OutputHandler javadoc](./images/doc_icon.png)](../javadoc/com/gitlab/jpp/OutputHandler.html "OutputHandler javadoc")

The OutputHandlers are the cement of a pipeline, they define the way in which data flows from one job to another.
The OutputHandler of a job is called after each task to process the output, commonly to build new tasks for the followings jobs.

The default implementation does nothing.

#### ExceptionHandler [![ExceptionHandler javadoc](./images/doc_icon.png)](../javadoc/com/gitlab/jpp/ExceptionHandler.html "ExceptionHandler javadoc")

If a exception is throw during a task execution, the job will catch it and delegate his processing to this module.

The default implementation will only print the stacktrace, it will not stop the pipeline execution or whatsoever.

#### EndOfJobAction

When a job end, this module is called. It contain a user-defined action that is guaranteed to be run after every tasks of the job. It can be useful to achieve a global processing over all the output of a job for example. Your action need to be defined as a [OneTimeAction](./getStarted.md#onetimeaction "OneTimeAction documentation").

If no EndOfJobAction is defined, a default implementation is used that does nothing.

## Pipeline class example

```java
public class PipelineImplementationExample extends Pipeline<String, String> {

   // Declare global data structure
   private final List<String> myGlobalBuffer;

   // Declare jobs
   private final Job jobA;
   private final Job jobB;

   public PipelineImplementationExample(int nbThreads) {
	   super(nbThreads);

	   // global data structure creation
	   this.myGlobalBuffer = new LinkedList<>();

	   // Jobs creation
	   // first job have no parents
	   this.jobA = new Job.JobBuilder(this, new ProcessorA(""))
			   // first job must wait on the startSignal
			   .setTaskSubmitter(new TaskHolder(this.startSignal))
			   // job output is used to build tasks for child job
			   .setOutputHandler(new OutputHandler<String>() {
				   @Override
				   public void handle(String processorOutput) {
					   if (processorOutput.matches("someString")) {
						   myGlobalBuffer.add(processorOutput);                            
					   }
					   jobB.buildNewTask(processorOutput);
				   }
			   })
			   .build();

	   // second job have jobA as parent
	   this.jobB = new Job.JobBuilder(this, new ProcessorB(), this.jobA)
			   .setOutputHandler(new OutputHandler<String>() {
				   @Override
				   public void handle(String processorOutput) {
					   // use the processor output and build the pipeline output with it
					   output += processorOutput;
				   }
			   })
			   .setEndOfJobAction(new OneTimeAction() {
				   @Override
				   protected void doAction() {
					   System.out.println("End of jobB");
				   }
			   })
			   .build();
   }

   @Override
   protected void reset() {
	   // clear and re-initialize all things that need it

	   this.myGlobalBuffer.clear();

	   // reset a job module that use the pipeline input as parameter
	   this.jobA.setProcessor(new ProcessorA(this.input));
   }

   @Override
   protected void initialTasks() {
	   // Create all initial tasks
	   this.jobA.buildNewTask(this.input);        
   }

   @Override
   protected void after() {
	   // process all result

	   for (String string : this.myGlobalBuffer) {
		   this.output += string;
	   }
   }
}
```

!!! note "About the OutputHandler"
	In this example I use anonymous classes for OutputHandlers. One could of course declare them in dedicated classes. But as OutputHandlers act as glue between processors and are often specifics to a pipeline, I find that having their implementation directly within the pipeline construction make it more readable/understable.
	The same logic can apply for other job's Modules.

## Pipeline usage example
```java
public static void main(String[] args) {
    // creating the pipeline
    PipelineImplementationExample pipeline = new PipelineImplementationExample(4);

    // using the pipeline
    String output = "";
    try {
        output = pipeline.executeFor("pipeline inputs parameters");
    } catch (InterruptedException ex) {
        Logger.getLogger(PipelineUsageExample.class.getName()).log(Level.SEVERE, null, ex);
    }

    System.out.println(output);

    // halting the pipeline
    pipeline.halt();
}
```

!!! warning
	Don't forget to halt the pipeline, otherwise your program mays stall indefinitely as the workers threads of the pipeline are still alive.

## Tooling

### ReusableCountDownLatch [![ReusableCountDownLatch javadoc](./images/doc_icon.png)](../javadoc/com/gitlab/jpp/ReusableCountDownLatch.html "ReusableCountDownLatch javadoc")

CountDownLatch is a native Java synchronization tools allowing threads to wait on the latch until his internal countdown reach zero. Any thread can decrement the count. This is useful to wait for a known number of events to happens before executing some code. A disadvantage is that a CountDownLatch instance can't be used more than once, which is annoying when reusing a Pipeline.

The ReusableCountDownLatch class intend to improve that point. It offer the same functionnality as a CountDownLatch but a instance can be reused, under the condition that the reset method is called.

### OneTimeAction [![OneTimeAction javadoc](./images/doc_icon.png)](../javadoc/com/gitlab/jpp/OneTimeAction.html "OneTimeAction javadoc")

A OneTimeAction is threadsafe class that define a action and guarantees it can be executed only once until reset.

A factory is provided to build actions that will only print text to console.

### Monitor [![Monitor javadoc](./images/doc_icon.png)](../javadoc/com/gitlab/jpp/Pipeline.Monitor.html "Monitor javadoc")

A monitor is provided with JPP to help keeping trace of a pipeline execution.
When enable, it will regularly print multiple infos about the pipeline state :

* The number of active threads vs the total number of threads.
* The number of pending tasks for the pipeline : the tasks waiting for a thread.
* The number of task done during this pipeline execution.
* The time elapsed since the start of the current pipeline execution.
* For each job in the pipeline :
	* If it have ended.
	* His TaskSubmitter status : HOLD if it is holding any tasks, CLEAR otherwise.
	* The number of pending task for the job.
	* The number of task done for the job.

To enable the Monitor the JVM must be launch with the following option : ```-Djpp_monitoring=true -Djpp_monitoring.stepTime=<seconds number>```.

Custom infos can be added to the monitor output by overriding the `getMonitoringMessage()` in extended Pipeline classes.
