/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

/**
 * Job module in charge of handling the incoming task.
 * @author Rémi Segretain
 */
public abstract class TaskSubmitter {
    
    /**
     * Factory method.
     * Build a basic task submitter which immediately queue the tasks to be executed by the job.
     * @return a task submitter instance.
     */
    public static TaskSubmitter defaultTaskSubmitterFactory() {
        return new TaskSubmitter() {
                    @Override
                    void submit(Task task) {
                        synchronized (this) {
                            this.submitToJob(task);
                        }
                    }

                    @Override
                    boolean isClear() {
                        synchronized (this) {
                            return true;
                        }
                    }
                };
    }
    
    /**
     * The job that will receive the task after them being processed by the task submiter.
     */
    protected Job job;
    
     /**
     * Reset the TaskSubmitter.
     * Call super() first if overrided.
     */
    void reset() {};
    
    /**
     * Entry point of tasks to the task submitter.
     * Submit a task to the task submitter.
     * @param task the task to handle.
     */
    abstract void submit(Task task);

    /**
     * Check if there is any task currently handled by the submitter.
     * @return true if the task submitter contains no tasks, false otherwise.
     */
    abstract boolean isClear();

    /**
     * Submit a task to the job in charge of executing it.
     * @param task the task to submit.
     */
    final void submitToJob(Task task) {
        this.job.submitTask(task);
    }
    
    /**
     * Set the job that will be in charge of executing the tasks processed by the task submiter.
     * @param job the job to use.
     */
    final void setJob(Job job) {
        this.job = job;
    }
}
