/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp.parameters;

import java.util.Objects;

/**
 * Wrap of String type in order to reuse the same instance when used as
 * parameter.
 * Threadsafe.
 *
 * @author Rémi Segretain
 */
public class StringParameter {

	private String text;

	public StringParameter(String str) {
		this.text = str;
	}

	public StringParameter() {
		this("");
	}

	/**
	 * @return the text value of this parameter.
	 */
	public String getText() {
		return text;
	}

	public synchronized void setText(String text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "StringParameter{" + "str=" + this.text + '}';
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 31 * hash + Objects.hashCode(this.text);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final StringParameter other = (StringParameter) obj;
		if (!Objects.equals(this.text, other.text)) {
			return false;
		}
		return true;
	}
}
