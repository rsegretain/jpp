/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A pipeline describe a process step by step and
 * automatically parallelize the execution of this process.
 *
 * @author Rémi Segretain
 * @param <Input>  the type of Input parameter required for a execution of the
 *                 pipeline.
 * @param <Output> the type of Output data generated from a execution of the
 *                 pipeline.
 */
public abstract class Pipeline<Input, Output> {

	/**
	 * The input parameter for the current execution.
	 */
	protected Input input;
	/**
	 * The ouput data of the current execution.
	 */
	protected Output output;

	/**
	 * The number of threads used for a execution.
	 */
	protected final int nbThreads;

	/**
	 * The executor managing the threads workers and the pipeline tasks queue.
	 */
	private final ThreadPoolExecutor executor;

	/**
	 * Latch used to signal that a execution can be launch.
	 */
	protected final ReusableCountDownLatch startSignal;

	/**
	 * Latch used to detect the end of a execution.
	 */
	private CountDownLatch endOfPipeline;

	/**
	 * Name of the JVM property to set in order to enable the monitoring.
	 */
	public static final String MONITORING_PROPERTY = "jpp_monitoring";

	/**
	 * Name of the JVM property to set to choose the monitoring steptime.
	 */
	public static final String MONITORING_PROPERTY_SETTING = "jpp_monitoring.stepTime";

	/**
	 * Flag saving if the monitoring is enable.
	 */
	private boolean isMonitoringEnabled = Boolean.parseBoolean(
			System.getProperty(MONITORING_PROPERTY));

	/**
	 * Pipeline monitor.
	 */
	private Monitor monitor;

	/**
	 * Count the total number of tasks executed by the pipeline over all its
	 * completed execution.
	 */
	private long totalTaskDone = 0;

	/**
	 * Jobs of the pipeline.
	 * Each job is a step of the global process described by the pipeline.
	 */
	private final List<Step> steps;

	/**
	 * List of the ReusableCountDownLatch used in the pipeline.
	 */
	private final List<ReusableCountDownLatch> latchs;

	/**
	 * Pipeline constructor.
	 *
	 * @param nbThreads the number of threads the pipeline will use to
	 *                  parallelize its execution.
	 */
	public Pipeline(int nbThreads) {
		this.nbThreads = nbThreads;
		this.executor = new ThreadPoolExecutor(
				nbThreads,
				nbThreads,
				1,
				TimeUnit.MINUTES,
				new LinkedBlockingQueue<>(),
				new ThreadFactory() {
			@Override
			public Thread newThread(Runnable r) {
				Thread t = new Thread(r);
				t.setDaemon(true);
				return t;
			}
		}
		);
		this.steps = new ArrayList<>();
		this.latchs = new ArrayList<>();
		this.startSignal = this.getNewReusableCountDownLatch(1);

		this.isMonitoringEnabled = Boolean.parseBoolean(System.getProperty(
				MONITORING_PROPERTY));
	}

	/**
	 * Execute the pipeline using the given input parameter.
	 * Blocking method :
	 * - Wait for a potential other execution to be done before starting a new
	 * one.
	 * - Wait for the execution to be complete before return.
	 *
	 * @param input the input parameter.
	 *
	 * @return the output data of the process.
	 *
	 * @throws InterruptedException  if the current thread is interrupted during
	 *                               the pipeline execution.
	 * @throws IllegalStateException if this method is called after the pipeline
	 *                               have been shutdown.
	 */
	public synchronized Output executeFor(Input input) throws InterruptedException, IllegalStateException {

		// check the pipeline is alive.
		if (this.executor.isShutdown()) {
			throw new IllegalStateException(
					"execution called after pipeline shutdown");
		}

		this.input = input;
		this.output = null;

		if (this.isMonitoringEnabled) {
			// create the monitor for this execution
			this.monitor = new Monitor();
		}

		// PIPELINE INIT

		// Reset of the global structures and input parameters
		this.reset();
		// reset of the steps
		for (int i = this.steps.size() - 1; i >= 0; i--) {
			this.steps.get(i).reset();
		}
		// reset of the countdownlatch, must be done after pipeline and job reset to avoid deadlocks during execution
		for (ReusableCountDownLatch latch : this.latchs) {
			latch.reset();
		}

		// call to user-defined methods to set the initials tasks
		this.initialTasks();


		if (isMonitoringEnabled) {
			// start the monitoring
			this.monitor.start();
		}

		// init the latch counter with the number of steps in the pipeline.
		this.endOfPipeline = new CountDownLatch(this.steps.size());

		// unleash the beast : signal to steps that the execution can begin.
		this.startSignal.countDown();

		// Wait for all the steps to end.
		// every minute if there is no active workers, check if steps are over
		// to prevent a edge case where end of a step is never checked.
		while (this.endOfPipeline.getCount() > 0) {
			this.endOfPipeline.await(1, TimeUnit.MINUTES);
			if (this.executor.getActiveCount() <= 0) {
				for (Step step : this.steps) {
					step.checkIfTheStepIsOver();
				}
			}
		}

		if (isMonitoringEnabled) {
			// stop the monitoring
			this.monitor.halt();
		}

		// call to user-defined methods to perform post-pipeline processes
		this.after();

		// update the number of tasks done
		this.totalTaskDone += (this.executor.getCompletedTaskCount() - this.totalTaskDone);

		return this.output;
	}

	/**
	 * Halt the pipeline whatever his state.
	 */
	public void halt() {
		synchronized (this.executor) {
			if (!this.executor.isShutdown()) {
				//halt every workers
				if (!this.executor.shutdownNow().isEmpty()) {
					System.err.println("Pipeline execution halted before end");
				}

				// unlock thread that started a execution
				for (Step job : this.steps) {
					this.endOfPipeline.countDown();
				}

				// halting monitor
				if (isMonitoringEnabled) {
					this.monitor.halt();
				}
			}
		}
	}

	/**
	 * Called before any pipeline execution.
	 * Reset here any variable or job module that need
	 * it before a new pipeline execution.
	 */
	protected abstract void reset();

	/**
	 * Called before any pipeline execution.
	 * You must create here the initial tasks,
	 * those for the roots jobs.
	 */
	protected abstract void initialTasks();

	/**
	 * Override if needed.
	 * Called after all jobs have ended and just before the return of a
	 * execution.
	 * Put here any needed global output processing.
	 */
	protected void after() {
	}

	;

    /**
     * Add a step to the pipeline.
     * @param step the step to add.
     */
	void addStep(Step step) {
		synchronized (this.steps) {
			this.steps.add(step);
			for (Object previousStep : step.previousSteps) {
				((Step) previousStep).followingSteps.add(step);
			}
		}
	}

	/**
	 * Submit a task to the pipeline executor.
	 *
	 * @param task
	 */
	void addTask(Task task) {
		try {
			synchronized (this.executor) {
				this.executor.execute(task);
			}
		} catch (RejectedExecutionException e) {
			e.printStackTrace();
			System.err.println(task);
		}
	}

	/**
	 * Signal that a step is over
	 */
	void onStepOver() {
		synchronized (this.steps) {
			this.endOfPipeline.countDown();
		}
	}

	/**
	 * Build and return a new ReusableCountDownLatch that will be managed by the
	 * pipeline
	 *
	 * @param count the count used to init the latch
	 *
	 * @return a new ReusableCountDownLatch
	 */
	public final synchronized ReusableCountDownLatch getNewReusableCountDownLatch(
			int count) {
		ReusableCountDownLatch latch = new ReusableCountDownLatch(count);
		this.latchs.add(latch);
		return latch;
	}

	/**
	 * Override this method add custom message
	 * to be displayed by the monitor.
	 *
	 * @return message.
	 */
	protected String getMonitoringMessage() {
		return "";
	}

	/**
	 * Gives the number of threads used by the pipeline
	 *
	 * @return
	 */
	int getNbThreads() {
		return this.nbThreads;
	}

	/**
	 * If monitoring is enable, display infos in console every n seconds.
	 * Set the property '
	 * {@value com.gitlab.jpp.Pipeline#MONITORING_PROPERTY}=true ' to enable the
	 * monitor.
	 * The monitor display informations every 5s by default,
	 * this duration can be set with the property '
	 * {@value com.gitlab.jpp.Pipeline#MONITORING_PROPERTY_SETTING}=seconds'
	 * where seconds is a integer.
	 */
	protected class Monitor extends Thread {

		private final int stepTime;
		private final CountDownLatch monitorStop;
		private long startTime = 0;

		public Monitor() {
			this.monitorStop = new CountDownLatch(1);
			String stepTimeProperty = System.getProperty(
					MONITORING_PROPERTY_SETTING);
			if (stepTimeProperty != null) {
				this.stepTime = Integer.parseInt(stepTimeProperty);
			} else {
				this.stepTime = 5;
			}
		}

		@Override
		public void run() {
			this.startTime = System.currentTimeMillis();
			try {
				do {
					this.display();
				} while (!monitorStop.await(stepTime, TimeUnit.SECONDS));

			} catch (InterruptedException ex) {
				Logger.getLogger(Pipeline.class.getName()).log(Level.SEVERE,
						null, ex);
			}
		}

		private void display() {
			// pipeline monitor.
			Runtime r = Runtime.getRuntime();
			String monitorMessage = "";
			monitorMessage += "Active threads : " + executor.getActiveCount()
					+ "\tThreads in pool : " + executor.getPoolSize()
					+ "\n Active Steps : " + endOfPipeline.getCount()
					+ "\nRAM : " + r.totalMemory() / 1000000L + "/" + r.maxMemory() / 1000000L + "Mo"
					+ "\nPendingTasks : " + executor.getQueue().size()
					+ "\tTasks done : " + (executor.getCompletedTaskCount() - totalTaskDone)
					+ "\nTime elapsed : " + ((System.currentTimeMillis() - this.startTime) / 1000) + "s"
					+ "\n";

			// custom pipeline infos.
			monitorMessage += getMonitoringMessage();

			// steps infos.
			for (Step step : steps) {
				monitorMessage += step.getMonitoringMessage();
			}

			// output
			System.out.println(monitorMessage);
		}

		public void halt() {
			this.display();
			this.monitorStop.countDown();
		}
	}
}
