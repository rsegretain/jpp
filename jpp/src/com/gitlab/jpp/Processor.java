/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

/**
 * Job module containing the processing done by the job.
 * @author Rémi Segretain
 * @param <Input> The type of the input parameter of the processor.
 * @param <Output> The type of a execution output.
 */
public interface Processor<Input, Output> {
    /**
     * Core method of a processor.
     * Contains the processing of a job
     * Warning : This method implementation must be thread-safe as it could be called multiple time simultaneously.
     * @param input the input parameter to process.
     * @return the output data.
     */
    public Output process(Input input);
    
    /**
     * Method called upon pipeline reset.
     */
    public void reset();
}
