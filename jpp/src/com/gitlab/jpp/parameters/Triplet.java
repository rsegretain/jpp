/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp.parameters;

import java.util.Objects;

/**
 * Generic structure combining three type as one.
 * Useful to regroup multiple parameters.
 *
 * @author Rémi Segretain
 * @param <First>
 * @param <Second>
 * @param <Third>
 */
public class Triplet<First, Second, Third> {

	public First first;
	public Second second;
	public Third third;

	public Triplet(First first, Second second, Third third) {
		this.first = first;
		this.second = second;
		this.third = third;
	}

	@Override
	public String toString() {
		return "Triplet{"
				+ "first(" + first.getClass() + ")=" + first
				+ ", second(" + second.getClass() + ")=" + second
				+ ", third(" + third.getClass() + ")=" + third + '}';
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 37 * hash + Objects.hashCode(this.first);
		hash = 37 * hash + Objects.hashCode(this.second);
		hash = 37 * hash + Objects.hashCode(this.third);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Triplet<?, ?, ?> other = (Triplet<?, ?, ?>) obj;
		if (!Objects.equals(this.first, other.first)) {
			return false;
		}
		if (!Objects.equals(this.second, other.second)) {
			return false;
		}
		if (!Objects.equals(this.third, other.third)) {
			return false;
		}
		return true;
	}
}
