# Installation

The library is available as a JAR file in the [gitlab repository](https://gitlab.com/rsegretain/jpp) to add manually in your project.

Direct download : [jpp.jar](https://gitlab.com/rsegretain/jpp/-/raw/master/jpp.jar).

In the future it *may* be available through maven.
