/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

/**
 * Job module in charge of handling the output of a processor execution
 * and feed it to the following jobs in the pipeline.
 * @author Rémi Segretain
 * @param <Output> the type of data outputed by a execution of the job's processor.
 */
public abstract class OutputHandler<Output> {
    /**
     * Handle the output data to feed them to the next jobs of the pipeline.
     * @param output the output data.
     */
    public abstract void handle(Output output);
    /**
     * Called upon pipeline reset.
     */
    public void reset(){};
}
