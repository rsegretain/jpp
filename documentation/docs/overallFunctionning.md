# Functioning
---

!!! note
	![Icon of direct javadoc link](./images/doc_icon.png "Icon of direct javadoc link") : Directs links to the corresponding javadoc.

## Work division

A Pipeline represent the global process we want parallel. Internally, this process is described as a succession of multiple sub-processes that the user need to define, we call those sub-processes [Processors ![Processor javadoc](./images/doc_icon.png "Processor javadoc")](../javadoc/com/gitlab/jpp/Processor.html "Processor javadoc").

## Parallelized execution

A processor will typically do what is done inside large loops in a linear code. In a pipeline each loop iteration will correspond to a Pipeline Task, a set of parameters used to execute a processor once. At runtime, instead of executing one loop iteration after another, multiple tasks can be run simultaneously and for diffents processors.

## Organisation

### Job

The execution of each processor will be managed by a [Job ![Job javadoc](./images/doc_icon.png "Job javadoc")](../javadoc/com/gitlab/jpp/Job.html "Job javadoc"). A job can be seen as a socket that take care of its chip and link it to the others devices of the system. In practice a job handle :

* the synchronization with the others jobs and the pipeline.
* the creation of new tasks.
* a queue of tasks.
* multiple [modules](getStarted.md#job-modules "JobModules documentation") that can be defined by the user to configure the execution of the processor :
	* Waiting conditions for tasks.
	* Handling of the output and the errors.
	* Execution of a final processing when the job end.

	!!! note
	 	A job will end when all his parents are ended and it don't have any pending task left.

![Shematic view of a job](./images/JobModules.jpg)

### Pipeline

 A pipeline need to know the processors execution order so the jobs order. Each job can have multiple parents and multiple childs forming a graph of jobs.

 ![Jobs graph example](./images/GraphOfJobExample.png)

 *Jobs graph example*

 A jobs graph is a directed acyclic graph, which means among others that it can have multiples starting or ending points and can't have any cycle.

!!! warning
 	Consequently the output from a job can **only** be used as input **for child jobs** and **never for parent jobs or itself**.

#### Priority

The policy implemented by JPP is to prioritize the tasks of the deepest jobs in the graph, in order to limit the accumulation of tasks and try to reduce the RAM comsumption.

#### Pipeline as a service

Pipeline are designed to be reusable instances to reduced the initialization cost when a called intensively. A pipeline is created, can be called any number of times and **need to be shutdown for the program to end**.
