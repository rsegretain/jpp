/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

import java.lang.Thread.State;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of a TaskSubmitter.
 * The task processed by a TaskHolder will be queued while a go-signal isn't
 * given.
 * After the go-signal is received all holded tasks are submitted to the job
 * and every subsequent task handled by the taskHolder will be directly
 * submitted to the job.
 *
 * The taskHolder can be reused across multiple pipeline execution
 * if the ReusableCountDownLatch used to send the go-signal is properly reset.
 *
 * @author Rémi Segretain
 */
public class TaskHolder extends TaskSubmitter {

	/**
	 * List of holded tasks.
	 */
	private final List<Task> holdedTasks;

	/**
	 * Used to receive the go-signal to the taskHolder.
	 */
	private final ReusableCountDownLatch barrier;

	/**
	 * Thread in charge of listening to the go-signal
	 * and of submitting the tasks and it is receveid.
	 */
	private final Waiter waiter;

	/**
	 * Flag saving the state of the taskHolder.
	 * False means the go-signal has not been received yet so tasks may be
	 * holded.
	 */
	private boolean isClear = false;

	/**
	 * Constructor
	 *
	 * @param barrier the latch used to receive the go-signal.
	 */
	public TaskHolder(ReusableCountDownLatch barrier) {
		this.holdedTasks = new LinkedList<>();
		this.barrier = barrier;
		this.waiter = new Waiter();
	}

	/**
	 * Submit a task to the taskHolder.
	 * The task will be holded if the go-signal hasn't been received yet.
	 * Otherwise the task will be directly handed to the job.
	 *
	 * Call may be blocking.
	 *
	 * @param task the task to submit.
	 */
	@Override
	void submit(Task task) {
		synchronized (this.holdedTasks) {
			if (this.barrier.getCount() > 0) {
				this.holdedTasks.add(task);
			} else {
				this.submitToJob(task);
			}
		}
	}

	/**
	 * Reset the taskHolder.
	 * Warning : this does method do not reset the ReusableCountDownLatch used
	 * to receive the go-signal.
	 */
	@Override
	void reset() {
		//check if the waiter have been started yet
		if (this.waiter.getState() == State.NEW) {
			// start the waiter and wait for him to be properly started
			synchronized (this.waiter) {
				this.waiter.start();
				try {
					this.waiter.wait();
				} catch (InterruptedException ex) {
					Logger.getLogger(TaskHolder.class.getName()).log(
							Level.SEVERE, null, ex);
				}
			}
		}
		this.isClear = false;
	}

	/**
	 * Indicates the state of the taskHolder.
	 *
	 * @return True if the go-signal has been receveid
	 *         and all holded tasks has been handed to the job,
	 *         False otherwise.
	 */
	@Override
	boolean isClear() {
		synchronized (this.holdedTasks) {
			return this.isClear;
		}
	}

	/**
	 * Thread in charge of waiting for the go-signal and of submitting the
	 * holded task to the job.
	 * Can be reused.
	 * Is a daemon : don't block the JVM halt.
	 */
	private class Waiter extends Thread {

		/**
		 * Constructor
		 */
		public Waiter() {
			this.setDaemon(true);
		}

		/**
		 * Wait for the latch to be reset, then for the go-signal and then
		 * submit all the holded tasks to the job.
		 */
		@Override
		public void run() {
			while (true) {
				try {
					// waiting for barrier to be reset
					synchronized (barrier) {
						// unlock the thread that started the waiter
						synchronized (this) {
							this.notifyAll();
						}
						barrier.wait();
					}
					//waiting for go signal
					barrier.await();
					synchronized (holdedTasks) {
						for (Task task : holdedTasks) {
							submitToJob(task);
						}
						isClear = true;
						if (holdedTasks.isEmpty()) {
							job.checkIfTheStepIsOver();
						}
						holdedTasks.clear();
					}
				} catch (InterruptedException ex) {
					Logger.getLogger(TaskHolder.class.getName()).log(
							Level.SEVERE, null, ex);
				}
			}
		}
	}
}
