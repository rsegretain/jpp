/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Contain a user-defined action.
 * It is garanted the action can be executed only once until reset.
 * Threadsafe.
 *
 * @author Rémi Segretain
 */
public abstract class OneTimeAction {

    /**
     * The void action, does nothing.
     */
    public static final OneTimeAction NOP = 
            new OneTimeAction() {
                    @Override
                    protected void doAction() {}
            };
    
    /**
     * Factory method.
     * Build a action that will output the given string.
     * @param str the string to display
     * @return OneTimeAction instance
     */
    public static OneTimeAction printActionFactory(String str) {
        return  new OneTimeAction() {
                    @Override
                    protected void doAction() {
                        System.out.println(str);
                    }
                };
    }
    
    /**
     * Flag saving the action state : executed or not
     */
    private final AtomicBoolean isActionDone = new AtomicBoolean(false);
    
    /**
     * Reset the action.
     * The action can be executed once again after this method.
     * Call super() first if overrided.
     */
    protected synchronized void reset() {
        this.isActionDone.set(false);
    }
    
    /**
     * Execute the action if it was not already executed, else do nothing.
     */
    public final void run() {
        if (isActionDone.compareAndSet(false, true)) {
            this.doAction();
        }
    }
    
    /*     
     * Method describing the action to perform.
     */
    protected abstract void doAction();
}
