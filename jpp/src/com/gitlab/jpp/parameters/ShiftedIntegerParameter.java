/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp.parameters;

/**
 * Wrap of Integer type in order to reuse the same instance when used as parameter.
 * The value of this parameter is defined as a shift from a other one.
 * All the modification made to this parameter affect the shift value, not the base value.
 * Threadsafe.
 * @author Rémi Segretain
 */
public class ShiftedIntegerParameter extends IntegerParameter {
    
    private final IntegerParameter base;
    
    public ShiftedIntegerParameter(IntegerParameter base, int offset) {
        super(offset);
        this.base = base;
    }
    
    public ShiftedIntegerParameter(IntegerParameter base) {
        this(base, 0);
    }

    /**
     * @return the base parameter of this one.
     */
    public IntegerParameter getBase() {
        return this.base;
    }

    /**
     * Return the value of the base parameter shifted by the value of this parameter.
     * @return 
     */
    @Override
    public int getValue() {
        return super.getValue() + this.base.getValue();
    }

    @Override
    public String toString() {
        return "ShiftedIntParameter{value=" + this.getValue() + " base=" + this.base + " offset=" + super.getValue() + '}';
    }
}
