# JPP : Java-Parallel-Pipeline

JPP is a library designed to help building a parallelized process that, when executed, will automatically distribute the workload across a pool of threads.

Handmade parallelization require rigor and time do achieve it properly. JPP intend to greatly reduce the time, the effort and the knowledge needed.

The primary objective of JPP is to facilitate software development for scientific research. In this context, code specifications can change rapidly and it is difficult to maintain a parallel software. JPP offers the possibility to easily scatter the different steps of a process across independent and reusable blocks of code, thus reducing the dependency between these blocks and limiting the impact of code refactoring. It also allows new processes to be built very quickly from existing blocks.

---
The sources are available in this [gitlab repository](https://gitlab.com/rsegretain/jpp)
