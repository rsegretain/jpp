/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */


import com.gitlab.jpp.Job;
import com.gitlab.jpp.OneTimeAction;
import com.gitlab.jpp.OutputHandler;
import com.gitlab.jpp.OutputsMerger;
import com.gitlab.jpp.Pipeline;
import com.gitlab.jpp.Processor;
import com.gitlab.jpp.parameters.Pair;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rémi Segretain
 */
public class TestOutputsMergerPipeline extends Pipeline<Integer, List<Integer>> {
    
    private final Job a;
    private final Job b;
    private final OutputsMerger<Integer,Integer,Integer> outputsMerger;
    private final Job c;

    private class OperandListProcessor implements Processor<Integer, List<Integer>> {
        @Override
        public List<Integer> process(Integer input) {
            Random rdm = new Random();
            List<Integer> operands = new ArrayList<>(input);

            for (int i = 0; i < input; i++) {
                //operands.add(rdm.nextInt());
                operands.add(i);
            }
            return operands;
        }

        @Override
        public void reset() {}
    }
    
    public TestOutputsMergerPipeline() {
        super(Runtime.getRuntime().availableProcessors());
        
        this.a = new Job.JobBuilder<>(
                this,
                new OperandListProcessor()
        )
        .setOutputHandler(
                new OutputHandler<List<Integer>>() {
                    @Override
                    public void handle(List<Integer> output) {
                        for (int i = 0; i < output.size(); i++) {
                            outputsMerger.handleDataA(i, output.get(i));
                        }
                    }
                }
        )
        .build();
        
        this.b = new Job.JobBuilder<>(
                this,
                new OperandListProcessor()
        )
        .setOutputHandler(
                new OutputHandler<List<Integer>>() {
                    @Override
                    public void handle(List<Integer> output) {
                        for (int i = 0; i < output.size(); i++) {
                            outputsMerger.handleDataB(i, output.get(i));
                        }
                    }
                }
        )
        .build();
        
        this.outputsMerger = new OutputsMerger.Builder(this, a, b)
                .setOutputHandler(
                        new OutputHandler<Pair<Integer,Integer>>() {
                            @Override
                            public void handle(Pair<Integer,Integer> output) {
                                c.buildNewTask(output);
                            }
                        }
                )
                .build();
        
        this.c = new Job.JobBuilder<>(
                this,
                new Processor<Pair<Integer, Integer>, Integer>() {
                    @Override
                    public Integer process(Pair<Integer, Integer> input) {
                        return input.first + input.second;
                    }

                    @Override
                    public void reset() {}
                },
                this.outputsMerger
        )
        .setEndOfJobAction(OneTimeAction.printActionFactory("EndOFJobC"))
        .build();
        
    }

    @Override
    protected void initialTasks() {
        this.a.buildNewTask(this.input);
        this.b.buildNewTask(this.input);
    }
    
    @Override
    protected void reset() {}
    
    public static void main(String[] args) {
        
        //System.setProperty(MONITORING_PROPERTY, "true");
        //System.setProperty(MONITORING_PROPERTY_SETTING, "1");
        
        TestOutputsMergerPipeline pipeline = new TestOutputsMergerPipeline();
        
        try {
            for (int i = 0; i < 10000; i++) {
                pipeline.executeFor(1000);
            }
        } catch (InterruptedException | IllegalStateException ex) {
            Logger.getLogger(TestOutputsMergerPipeline.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        pipeline.halt();
    }
}
