/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * Wrapper of a CountDownLatch.
 * Allowing to reuse a latch object, after a reset.
 * @author Rémi Segretain
 */
public class ReusableCountDownLatch {
    /**
     * The CountDownLatch used in background.
     */
    private CountDownLatch latch;
    /**
     * The number of signal needed to release the latch.
     */
    private final int count;

    /**
     * Constructor
     * @param count The number of signal needed to release the latch.
     */
    ReusableCountDownLatch(int count) {
        this.count = count;
        this.latch = new CountDownLatch(this.count);
    }

    /**
     * Reset the CountDownLatch.
     * Unlock any thread waiting on the old latch.
     * After reset, notify all thread waiting on this ReusableCountDownLatch instance.
     */
    synchronized void reset() {
        CountDownLatch oldLatch = this.latch;
        // reset the latch
        this.latch = new CountDownLatch(this.count);
        // free any remaining waiting threads
        while (oldLatch.getCount() > 0) {
            oldLatch.countDown();
        }
        this.notifyAll();
    }
    
    /**
     * Wait for the latch to be released.
     * Blocking method.
     * @throws InterruptedException if the current thread is interrupted while waiting.
     */
    public void await() throws InterruptedException {
        this.latch.await();
    }
    
    /**
     * Wait etheir for the latch to be released or the time specified.
     * @param timeout the duration to wait.
     * @param unit the timeunit of the duration.
     * @return true if the count reached zero and false if the waiting time elapsed before the count reached zero.
     * @throws InterruptedException if the current thread is interrupted while waiting.
     */
    public boolean await(long timeout, TimeUnit unit) throws InterruptedException {
        return this.latch.await(timeout, unit);
    }
    
    /**
     * Return the initial count of the latch.
     * @return the initial count of the latch. 
     */
    public int getInitialCount() {
        return this.count;
    }
    
    /**
     * Return the current count of the latch.
     * @return the current count of the latch.
     */
    public long getCount() {
        return this.latch.getCount();
    }
    
    /**
     * Wrap call to {@link java.util.concurrent.CountDownLatch#countDown()}
     * @see java.util.concurrent.CountDownLatch#countDown()
     */
    public void countDown() {
        this.latch.countDown();
    }

    @Override
    public String toString() {
        return "ReusableCountDownLatch{" + "latch=" + latch + ", count=" + count + '}';
    }
}
