/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp.parameters;

/**
 * Wrap of Double type in order to reuse the same instance when used as
 * parameter.
 * Threadsafe.
 *
 * @author Rémi Segretain
 */
public class DoubleParameter {

	private double value;

	public DoubleParameter(double value) {
		this.value = value;
	}

	public DoubleParameter() {
		this(0.0);
	}

	public double getValue() {
		return this.value;
	}

	public synchronized void setValue(double value) {
		this.value = value;
	}

	/**
	 * Add the parameter to the current value.
	 *
	 * @param valueAdded
	 */
	public synchronized void add(double valueAdded) {
		this.value += valueAdded;
	}

	/**
	 * Substract the parameter from the current value.
	 *
	 * @param valueSubtracted
	 */
	public synchronized void sub(double valueSubtracted) {
		this.value -= valueSubtracted;
	}

	/**
	 * Increment the current value by one.
	 */
	public synchronized void inc() {
		this.value++;
	}

	/**
	 * Decrement the current value by one.
	 */
	public synchronized void dec() {
		this.value--;
	}

	@Override
	public String toString() {
		return "DoubleParameter{" + "value=" + this.value + '}';
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 53 * hash + (int) (Double.doubleToLongBits(this.getValue()) ^ (Double.doubleToLongBits(
				this.getValue()) >>> 32));
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}

		if (!DoubleParameter.class.isInstance(this) || !DoubleParameter.class.isInstance(
				obj)) {
			return false;
		}

		final DoubleParameter other = (DoubleParameter) obj;
		return Double.doubleToLongBits(this.getValue()) == Double.doubleToLongBits(
				other.getValue());
	}
}
