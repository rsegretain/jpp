/*
MIT License

Copyright (c) 2021 Rémi Segretain

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
package com.gitlab.jpp;

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A Job is one possible Step of a Pipeline.
 * It manage the execution of tasks for a specific Processor.
 * It take care of the synchronization of those executions
 * with the others steps of the pipeline.
 *
 * @param <Input>  the type of Input parameter required for the execution
 *                 managed by the job.
 * @param <Output> the type of Output data generated from the execution managed
 *                 by the job.
 *
 * @author Rémi Segretain
 */
public final class Job<Input, Output> extends Step<Output> {

	/**
	 * The modules of the job.
	 */
	private final Processor<Input, Output> processor;
	private final ExceptionHandler exceptionHandler;
	private final TaskSubmitter taskSubmitter;
	private final OneTimeAction endOfJobAction;

	/**
	 * The queue of tasks waiting to be executed.
	 */
	private final LinkedList<Task> waitingTasksQueue;

	/**
	 * Count the number of pending tasks for the job.
	 * This number is the sum of the task in the job queue and in the pipeline
	 * executor queue.
	 */
	private final AtomicInteger pendingTasks;

	/**
	 * Count the number of task currently submitted to the pipeline queue
	 */
	private final AtomicInteger tasksSubmittedToPipeline;

	/**
	 * Save the limit of tasks that can be executed in parallel for this job.
	 */
	private final int maximumParallelTasks;

	/**
	 * Count the number of tasks executed by the job during a pipeline
	 * execution.
	 */
	private long endedTasks;

	/**
	 * Signal the end of the job to anyone waiting for it.
	 */
	private final ReusableCountDownLatch endOfJobLatch;

	/**
	 * Constructor.
	 * Require the pipeline, the parents jobs and the modules.
	 *
	 * @param pipeline
	 * @param processor
	 * @param outputHandler
	 * @param exceptionHandler
	 * @param taskSubmitter
	 * @param previousJob      the parents jobs in the pipeline graph of jobs.
	 * @param endOfJobAction
	 */
	private Job(Pipeline pipeline,
				Processor<Input, Output> processor,
				OutputHandler<Output> outputHandler,
				ExceptionHandler exceptionHandler,
				TaskSubmitter taskSubmitter,
				int maximumParallelTasks,
				OneTimeAction endOfJobAction,
				Step... previousStep) {
		super(pipeline, outputHandler, previousStep);

		this.taskSubmitter = taskSubmitter;
		this.maximumParallelTasks = maximumParallelTasks;
		this.processor = processor;
		this.exceptionHandler = exceptionHandler;

		this.waitingTasksQueue = new LinkedList<>();
		this.pendingTasks = new AtomicInteger(0);
		this.tasksSubmittedToPipeline = new AtomicInteger(0);
		this.endedTasks = 0;

		this.endOfJobAction = endOfJobAction;
		this.endOfJobLatch = this.pipeline.getNewReusableCountDownLatch(1);
	}

	/**
	 * Reset the job and his modules to be ready for a new pipeline execution
	 */
	synchronized void reset() {
		super.reset();
		this.waitingTasksQueue.clear();
		this.pendingTasks.set(0);
		this.endedTasks = 0;

		this.endOfJobAction.reset();
		this.taskSubmitter.reset();
		this.processor.reset();
		this.exceptionHandler.reset();
	}

	/**
	 * Getter for the ReusableCountDownLatch signalling the end of this job.
	 *
	 * @return a ReusableCountDownLatch signalling the end of this job.
	 */
	public ReusableCountDownLatch getEndOfJobLatch() {
		return this.endOfJobLatch;
	}

	/**
	 * Execute task : call the job processor with the given input parameter.
	 *
	 * @param input the input parameter of the task.
	 */
	void executeFor(Input input) {
		try {
			this.outputHandler.handle(this.processor.process(input));
		} catch (Exception e) {
			this.exceptionHandler.handle(e);
		}

		synchronized (this) {
			this.tasksSubmittedToPipeline.decrementAndGet();
			this.pendingTasks.decrementAndGet();
			this.endedTasks++;
			this.checkIfTheStepIsOver();
		}
	}

	/**
	 * Create a new task for the job and give it to the TaskSubmitter of the
	 * job.
	 * The task will de refused if the job has already ended.
	 *
	 * @param input the input paramater for the new task.
	 */
	public void buildNewTask(Input input) {
		if (!this.stepOver) {
			this.pendingTasks.incrementAndGet();
			this.taskSubmitter.submit(new Task<>(this, input));
		}
	}

	/**
	 * Submit a task to the job if it has not already ended.
	 *
	 * @param task the submitted task.
	 */
	void submitTask(Task task) {
		if (!this.stepOver) {
			synchronized (this.waitingTasksQueue) {
				this.waitingTasksQueue.add(task);
			}
			this.trySubmitTasks();
		}
	}

	/**
	 * Try to submit task from the job queue to the pipeline queue.
	 * Childs jobs have priority.
	 */
	@Override
	void trySubmitTasks() {
		synchronized (this.waitingTasksQueue) {
			if (this.waitingTasksQueue.isEmpty()) {
				this.signalStepIsIdle();
			} else {
				if (this.followingSteps.stream().mapToInt(
						(step) -> step.getWaitingTasksNumber()).sum() <= 0
						&& this.tasksSubmittedToPipeline.get() < this.maximumParallelTasks) {
					this.tasksSubmittedToPipeline.incrementAndGet();
					this.pipeline.addTask(this.waitingTasksQueue.poll());
				}
			}
		}
	}

	/**
	 * Give the number of tasks holded in the job queue
	 *
	 * @return
	 */
	@Override
	int getWaitingTasksNumber() {
		return this.waitingTasksQueue.size();
	}

	/**
	 * Return true if the job have no more tasks to execute and if all parents
	 * have ended
	 *
	 * @return true if the job work is done
	 */
	@Override
	boolean isStepOver() {
		return (this.stepOver
				|| (this.previousSteps.stream()
						.map((step) -> step.isStepOver())
						.noneMatch((isStepOver) -> isStepOver == false)
				&& this.taskSubmitter.isClear()
				&& this.pendingTasks.get() <= 0));
	}

	/**
	 * Check if the Job is over
	 * and handle the required action if it is effectively over.
	 */
	@Override
	synchronized void checkIfTheStepIsOver() {
		if (this.isStepOver() && this.stepOver == false) {
			this.endOfJobAction.run();
			this.endOfJobLatch.countDown();
			this.onStepOver();
		} else if (this.waitingTasksQueue.isEmpty()) {
			this.signalStepIsIdle();
		} else {
			this.trySubmitTasks();
		}
	}

	/**
	 * Gives monitoring infos concerning the job.
	 *
	 * @return infos as string.
	 */
	@Override
	public String getMonitoringMessage() {
		return this.processor.getClass().getSimpleName()
				+ (this.stepOver ? " [DONE] :" : " :")
				+ "\tTaskSubmitter: " + (this.taskSubmitter.isClear() ? "CLEAR" : "HOLD")
				+ "\tPendingTasks : " + this.pendingTasks.get()
				+ "\tInPipelineTasks : " + this.tasksSubmittedToPipeline.get()
				+ "\tEndedTasks : " + this.endedTasks + "\n";
	}

	;

    @Override
	public String toString() {
		return this.getMonitoringMessage();
	}

	/**
	 * Build a job with the user specify parameters and fill the other with
	 * default values.
	 *
	 * @param <Input>  the type of Input parameter required for the execution
	 *                 managed by the job.
	 * @param <Output> the type of Output data generated from the execution
	 *                 managed by the job.
	 */
	public static class JobBuilder<Input, Output> {

		/**
		 * Evvery job constructor parameters.
		 */
		private final Pipeline pipeline;
		private final Processor<Input, Output> processor;
		private final Step[] previousSteps;

		private OutputHandler<Output> outputHandler;
		private ExceptionHandler exceptionHandler;
		private TaskSubmitter taskSubmitter;
		private int maximumParallelTasks;
		private OneTimeAction endOfJobAction;

		/**
		 * JobBuilder Constructor.
		 * Require all job parameter for wich there is no default value.
		 *
		 * @param pipeline      the pipeline containing the job.
		 * @param processor     the processor of the job.
		 * @param previousSteps varargs parameters, parents of the job.
		 */
		public JobBuilder(Pipeline pipeline, Processor<Input, Output> processor,
						  Step... previousSteps) {
			this.pipeline = pipeline;
			this.processor = processor;

			this.outputHandler = new OutputHandler<Output>() {
				@Override
				public void handle(Output output) {
				}
			};
			this.exceptionHandler = new ExceptionHandler() {
				@Override
				public void handle(Throwable t) {
					t.printStackTrace();
				}
			};
			if (previousSteps.length == 0) {
				this.taskSubmitter = new TaskHolder(this.pipeline.startSignal);
			} else {
				this.taskSubmitter = TaskSubmitter.defaultTaskSubmitterFactory();
			}
			this.maximumParallelTasks = pipeline.getNbThreads();
			this.previousSteps = previousSteps;
			this.endOfJobAction = OneTimeAction.NOP;
		}

		/**
		 * OutputHandler setter.
		 *
		 * @param outputHandler
		 *
		 * @return the jobBuilder instance for chaining method call.
		 */
		public JobBuilder setOutputHandler(OutputHandler<Output> outputHandler) {
			this.outputHandler = outputHandler;
			return this;
		}

		/**
		 * ExceptionHandler setter.
		 *
		 * @param exceptionHandler
		 *
		 * @return the jobBuilder instance for chaining method call.
		 */
		public JobBuilder setExceptionHandler(ExceptionHandler exceptionHandler) {
			this.exceptionHandler = exceptionHandler;
			return this;
		}

		/**
		 * TaskSubmitter setter.
		 *
		 * @param taskSubmitter
		 *
		 * @return the jobBuilder instance for chaining method call.
		 */
		public JobBuilder setTaskSubmitter(TaskSubmitter taskSubmitter) {
			if (previousSteps.length == 0) {
				Logger.getLogger(Pipeline.class.getName()).log(Level.WARNING,
						this.processor.getClass().getSimpleName() + " : Overriding TaskSubmitter for Job with no previous steps can cause errors and unexpected behaviours, be careful !");
			}
			this.taskSubmitter = taskSubmitter;
			return this;
		}

		/**
		 * Set the execution limit of parallel tasks for this job.
		 * The maximum limit is the number of threads of the pipeline.
		 * The minimum limit is one.
		 *
		 * @param maximumParallelTasks
		 *
		 * @return
		 */
		public JobBuilder setMaximumParallelTasks(int maximumParallelTasks) {
			if (maximumParallelTasks > 0 && maximumParallelTasks <= this.pipeline.getNbThreads()) {
				this.maximumParallelTasks = maximumParallelTasks;
			}
			return this;
		}

		/**
		 * EndOfJobAction setter.
		 *
		 * @param endOfJobAction
		 *
		 * @return the jobBuilder instance for chaining method call.
		 */
		public JobBuilder setEndOfJobAction(OneTimeAction endOfJobAction) {
			this.endOfJobAction = endOfJobAction;
			return this;
		}

		/**
		 * Build the job with the specified parameter.
		 *
		 * @return a new job.
		 */
		public Job<Input, Output> build() {
			Job job = new Job(this.pipeline, this.processor, this.outputHandler,
					this.exceptionHandler, this.taskSubmitter,
					this.maximumParallelTasks, this.endOfJobAction,
					this.previousSteps);
			this.pipeline.addStep(job);
			this.taskSubmitter.setJob(job);
			return job;
		}
	}
}
